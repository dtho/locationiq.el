# locationiq.el

*Interact with the LocationIQ HTTP API*

*locationiq.el** provides an Emcas interface for geocoding data via the LocationIQ API. 

---

### Getting started

Obtain a LocationIQ token.


### Configure locationiq.el

Set `locationiq-token`.


## Use

Use <kbd>M-x</kbd> `locationiq` for one-off use.



## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

If you did not receive a copy of the GNU General Public License along with this program, see http://www.gnu.org/licenses/.
