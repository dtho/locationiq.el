;;; locationiq.el --- Get LocationIQ location data -*- lexical-binding: t -*-

;; Copyright (C) 2020 David Thompson
;; Author: David Thompson
;; Version: 0.1
;; Keywords:
;; Homepage: https://github.com/thomp/locationiq
;; URL: https://github.com/thomp/locationiq
;; Package-Requires: ((request "0.2.0") (cl-lib "0.5") (emacs "24"))

;;; Commentary:

;; This package provides a way to interact with the LocationIQ Forward
;; Geocoding HTTP API.

;;; Code:

(require 'request)

(defgroup locationiq ()
  "Interact with the LocationIQ HTTP API."
  :group 'external)

(defcustom locationiq-token nil
  "The LocationIQ token."
  :group 'locationiq
  :type (string))

(defvar locationiq-response nil
  "The last API response.")

(defvar locationiq-response-data nil
  "The data associated with the last API response.")

(defvar locationiq-lon nil
  "The longitude value, if any, associated with the last API response.")

(defvar locationiq-lat nil
  "The latitude value, if any, associated with the last API response.")

(defun locationiq (&optional city state)
  (interactive)
  (let* ((completion-ignore-case t)
	 (final-city (or city
			 (read-from-minibuffer "City: ")))
	 (final-state (or state
			  (read-from-minibuffer "State: "))))
    (locationiq-call-api city state)
    ))

(cl-defun locationiq-http-callback (&key data response error-thrown &allow-other-keys)
  (setf locationiq-response-data data)
  (setf locationiq-response response)
  (let ((response-data (elt (json-read-from-string data) 0)))
    (setf locationiq-lat (locationiq-aval response-data 'lat))
    (setf locationiq-lon (locationiq-aval response-data 'lon))))

;; The Forward Geocoding API converts a street address to a location.

;; Region 1 (US) format:
;; GET https://us1.locationiq.com/v1/search.php?key=YOUR_PRIVATE_TOKEN&q=SEARCH_STRING&format=json

(defun locationiq-call-api (city state)
  (request
    "https://us1.locationiq.com/v1/search.php"
    :params (list (cons "key" locationiq-token)
		  (cons "city" city)
		  (cons "state" state)
		  (cons "format" "json"))
    :parser 'buffer-string
    :error (function*
	    (lambda (&key data error-thrown response symbol-status &allow-other-keys)
	      (setf locationiq-response response)
	      (message "data: %S " data)
	      (message "symbol-status: %S " symbol-status)
	      (message "E Error response: %S " error-thrown)
	      (message "response: %S " response)))
    :status-code '((500 . (lambda (&rest _) (message "Got 500 -- server seems unhappy"))))
    :success (function locationiq-http-callback)
    :type "GET"))

(defun locationiq-aval (alist key)
  "Utility function to retrieve value associated with key KEY in alist ALIST."
  (let ((pair (assoc key alist)))
    (if pair
	(cdr pair)
      nil)))
